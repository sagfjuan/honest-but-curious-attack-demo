package com.example.breno.myapplication;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class MessageService extends Service {

    public static ArrayList<String> messages = new ArrayList<String>();

    public Messenger mMessenger = new Messenger(new MessageHandler(this));

    public static final String KEY_MESSAGE = "KEY_MESSAGE";
    public static final String KEY_FREQ = "KEY_FREQUENCY";
    public static final String KEY_PKG = "KEY_PACKAGE";

    public MessageService() {}

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    public class MessageHandler extends Handler {

        WeakReference<Context> contextReference;

        public MessageHandler(Context context) {
            contextReference = new WeakReference<>(context);
        }

        @Override
        public void handleMessage(Message msg) {
            if (contextReference.get() != null) {
//                Log.d("MessageService", getClass().getName());
//                Log.d("MessageService", getClass().getPackage().getName());
//
//                messages.add(msg.getData().getString(KEY_MESSAGE));
//                Toast.makeText(contextReference.get(), "Array Size : " + messages.size(), Toast.LENGTH_SHORT).show();


            if(msg.getData().containsKey(KEY_MESSAGE)){
                messages.add(msg.getData().getString(KEY_MESSAGE));
                Toast.makeText(contextReference.get(), msg.getData().getString(KEY_MESSAGE), Toast.LENGTH_SHORT).show();
                Toast.makeText(contextReference.get(), "Array Size : " + messages.size(), Toast.LENGTH_SHORT).show();

            } else if (msg.getData().containsKey(KEY_FREQ)){

                int counter = 0;
                Bundle bundle = msg.getData();
                String temp = bundle.getString(KEY_FREQ);
                Log.d("MessageService", temp);
                Log.d("MessageService", ""+messages.size());
                for(int i = 0; i < messages.size(); i++){

                    if(messages.get(i).equals(temp)){
                        counter++;
                    }
                }

                Toast.makeText(contextReference.get(), "Frequency of messageArray is " + counter, Toast.LENGTH_SHORT).show();
                sendReply(msg.getData().getString(KEY_PKG), ""+counter);
            }
            }
        }



        public void sendReply(String intentFilterName, String message){
            Intent intent = new Intent(intentFilterName);
            intent.putExtra("ServiceReply", message);
            sendBroadcast(intent);

        }
    }

}
