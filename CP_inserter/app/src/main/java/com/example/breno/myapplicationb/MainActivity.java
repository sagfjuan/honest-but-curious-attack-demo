package com.example.breno.myapplicationb;

import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.nio.charset.Charset;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    static final String _ID = "_id";
    static final String NAME = "name";
    static final String GRADE = "grade";
    static final String PROVIDER_NAME = "com.example.breno.myapplication3.StudentsProvider";
    static final String URL = "content://" + PROVIDER_NAME + "/students";
    static final Uri CONTENT_URI = Uri.parse(URL);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        InsertContentProvider();
    }

    void getContentProvider(){

        Uri yourURI = Uri.parse("content://com.example.breno.myapplication3.StudentsProvider");
        ContentProviderClient yourCR = getContentResolver().acquireContentProviderClient(yourURI);
        System.out.println("@@1");

        try {
            Cursor yourCursor = yourCR.query(yourURI, null, null, null , null);
            System.out.println("@@@");
            if (yourCursor.moveToFirst()) {
                Toast.makeText(this, yourCursor.getString(yourCursor.getColumnIndex(_ID)) +
                                ", " +  yourCursor.getString(yourCursor.getColumnIndex(NAME)) +
                                ", " + yourCursor.getString(yourCursor.getColumnIndex(GRADE)),
                        Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    public String genString() {
        byte[] array = new byte[7]; // length is bounded by 7
        new Random().nextBytes(array);
        String generatedString = new String(array, Charset.forName("UTF-8"));
        return generatedString;
    }

    void InsertContentProvider(){

        ContentValues values = new ContentValues();
        values.put(NAME, genString());
        values.put(GRADE, genString());
        Uri uri = getContentResolver().insert(CONTENT_URI, values);

        Toast.makeText(getBaseContext(), uri.toString(), Toast.LENGTH_LONG).show();

    }



}
