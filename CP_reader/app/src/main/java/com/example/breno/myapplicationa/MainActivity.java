package com.example.breno.myapplicationa;

import android.content.ContentProviderClient;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    static final String _ID = "_id";
    static final String NAME = "name";
    static final String GRADE = "grade";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getContentProvider();
    }

    void getContentProvider(){

        Uri yourURI = Uri.parse("content://com.example.breno.myapplication3.StudentsProvider");
        ContentProviderClient yourCR = getContentResolver().acquireContentProviderClient(yourURI);
        System.out.println("@@1");

        try {

            Cursor yourCursor = yourCR.query(yourURI, null, null, null , null);
            System.out.println("@@@");
//            if (yourCursor.moveToFirst()) {
//                Toast.makeText(this, yourCursor.getString(yourCursor.getColumnIndex(_ID)) +
//                                    ", " +  yourCursor.getString(yourCursor.getColumnIndex(NAME)) +
//                                    ", " + yourCursor.getString(yourCursor.getColumnIndex(GRADE)),
//                            Toast.LENGTH_SHORT).show();
//
//            }

            if (yourCursor.moveToFirst()) {
                do{
                    Toast.makeText(this,
                            yourCursor.getString(yourCursor.getColumnIndex(_ID)) +
                                    ", " +  yourCursor.getString(yourCursor.getColumnIndex(NAME)) +
                                    ", " + yourCursor.getString(yourCursor.getColumnIndex(GRADE)),
                            Toast.LENGTH_SHORT).show();
                } while (yourCursor.moveToNext());
            }

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
