package evolution.lancer.breno.appservicetalker;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity {

    public Messenger mMessenger;
    public boolean isBound = false;
    BroadcastReceiver broadcastReceiver = null;
    public static final String KEY_MESSAGE = "KEY_MESSAGE";
    public static final String KEY_FREQ = "KEY_FREQUENCY";
    public static final String KEY_PKG = "KEY_PACKAGE";

    private void registerBroadcastReceivers(){
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle bundle = intent.getExtras();
                String arg1 = bundle.getString("ServiceReply");
                Log.d("MainActivity", arg1);
            }
        };

        IntentFilter intentFilter = new IntentFilter("evolution.lancer.breno.appservicetalker.ACTION_RESPONSE");
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_send_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBound) {
                    Message message = Message.obtain();
                    Bundle bundle = new Bundle();
                    bundle.putString(KEY_MESSAGE, "HELLO SERVICE. I AM ACTIVITY");
                    message.setData(bundle);
                    try {
                        mMessenger.send(message);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        findViewById(R.id.btn_send2_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBound) {
                    Message message = Message.obtain();
                    Bundle bundle = new Bundle();
                    bundle.putString(KEY_MESSAGE, "Radicola");
                    message.setData(bundle);
                    try {
                        mMessenger.send(message);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        findViewById(R.id.btn_frq_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBound) {
                    Message message = Message.obtain();
                    Bundle bundle = new Bundle();
                    bundle.putString(KEY_FREQ, "HELLO SERVICE. I AM ACTIVITY");
                    bundle.putString(KEY_PKG, "evolution.lancer.breno.appservicetalker.ACTION_RESPONSE");
                    message.setData(bundle);
                    try {
                        mMessenger.send(message);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        });


        findViewById(R.id.btn_frq2_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBound) {
                    Message message = Message.obtain();
                    Bundle bundle = new Bundle();
                    bundle.putString(KEY_FREQ, "Radicola");
                    bundle.putString(KEY_PKG, "evolution.lancer.breno.appservicetalker.ACTION_RESPONSE");
                    message.setData(bundle);
                    try {
                        mMessenger.send(message);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        registerBroadcastReceivers();

        //com.example.breno.myapplication.MessageService$MessageHandler
        Intent intent = new Intent("com.example.breno.myapplication.MessageService");
        //Log.d("MainActivity", "Before init intent.componentName");
        intent.setPackage("com.example.breno.myapplication");
        //intent.setComponent(new ComponentName("com.example.breno.myapplication", "MessageService"));
        intent.setAction("com.example.breno.myapplication.MessageService");
        bindService(intent, new Connection(this), BIND_AUTO_CREATE);
    }

    public static class Connection implements ServiceConnection {

        WeakReference<Context> contextReference;

        public Connection(Context context) {
            contextReference = new WeakReference<>(context);
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Context context = contextReference.get();
            if (context != null) {
                ((MainActivity) context).mMessenger = new Messenger(service);
                ((MainActivity) context).isBound = true;
                Toast.makeText(context, "Service Connected", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Context context = contextReference.get();
            if (context != null) {
                ((MainActivity) context).isBound = false;
                Toast.makeText(context, "Service Disconnected", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

